/*
* generated by Xtext
*/
package org.cmg.slcsmc.xtext.lcs.ui.outline

/**
 * Customization of the default outline structure.
 *
 * see http://www.eclipse.org/Xtext/documentation.html#outline
 */
class SpaceModelOutlineTreeProvider extends org.eclipse.xtext.ui.editor.outline.impl.DefaultOutlineTreeProvider {
	
}
