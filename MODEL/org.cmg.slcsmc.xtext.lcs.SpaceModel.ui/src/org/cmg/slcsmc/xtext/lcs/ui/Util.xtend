package org.cmg.slcsmc.xtext.lcs.ui

import java.util.HashMap
import org.cmg.slcsmc.logic.SLCSFormula
import org.cmg.slcsmc.logic.mc.SLCSModelChecker
import org.cmg.slcsmc.model.graphs.GraphClosureModel
import org.cmg.slcsmc.model.graphs.Node
import org.cmg.slcsmc.model.images.Pixel
import org.cmg.slcsmc.model.images.SWTImageClosureModel
import org.cmg.slcsmc.xtext.lcs.spaceModel.ColourPredicate
import org.cmg.slcsmc.xtext.lcs.spaceModel.GraphDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.ImageModel
import org.cmg.slcsmc.xtext.lcs.spaceModel.Model
import org.cmg.slcsmc.xtext.lcs.spaceModel.PixelPredicate
import org.cmg.slcsmc.xtext.lcs.spaceModel.SpaceFormulaDeclaration
import org.cmg.slcsmc.xtext.lcs.util.GraphModelGenerator
import org.cmg.slcsmc.xtext.lcs.util.SlcsFormulaGenerator
import org.eclipse.core.resources.IResource
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.ui.PlatformUI
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import com.google.inject.Inject
import org.cmg.slcsmc.xtext.lcs.spaceModel.LabelDeclaration
import org.eclipse.core.resources.IProject
import org.eclipse.core.resources.IFile

class Util {
	
	@Inject GraphModelGenerator gg;
	
	def  HashMap<IResource,Model> getActiveModels() {
		var references = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences()
		var xtextEditorList = references.map[it.getPart(false)].filter(typeof(XtextEditor))
		newHashMap( xtextEditorList.map[it.resource -> it.loadModel].filter[ it.value != null ] )
	}

	def  loadModel( XtextEditor editor ) {
		var content = editor.document.readOnly(
					new IUnitOfWork<EList<EObject>, XtextResource>(){

                        def override EList<EObject> exec(XtextResource state) throws Exception {
                                if (state.getErrors().size()>0) {
                                        return null;
                                }
                                return state.getContents();
                        }
                        
                }
		)
		if (( content != null)&&(content.size > 0)) {
			var model = content.get(0)
			if (model instanceof Model) {
				model
			} else {
				null
			}
		} else {
			null
		}

	}
	
	def  getGraphModels( Model m ) {
		m.elements.filter(typeof(GraphDeclaration)).map[it.name].toList
	}

	def  getImageModels( Model m ) {
		m.elements.filter(typeof(ImageModel)).map[it.name].toList
	}
	
	def  getGraphModel( Model m , String name ) {
//		var gg = new GraphModelGenerator();
		var model = m.elements.filter(typeof(GraphDeclaration)).findFirst[ it.name == name ]
		if (model != null) {
			gg.generateGraphModel( model , new HashMap<String,Integer>( ))
		} else {
			null
		}		
	}

	def getImageModel( IProject project , Model m , String name ) {
		var model = m.elements.filter(typeof(ImageModel)).findFirst[ it.name == name ]
		if (model != null) {
			var imgResource = project.findMember(model.file.file)
			if ((imgResource != null)&&(imgResource instanceof IFile)) {
				val imageModel = new SWTImageClosureModel( (imgResource as IFile).contents );
				model.labels.forEach[ 
					it.predicate.computeColorFilter( it.label.name , imageModel );
				]
				imageModel;				
			} else {
				null;				
			}
		} else {
			null
		}	
	}
	
	def dispatch computeColorFilter( ColourPredicate predicate , String name , SWTImageClosureModel model ) {
		model.setFilter(name,predicate.red,predicate.green,predicate.blue, predicate.tolerance );
	}	

	def dispatch computeColorFilter( PixelPredicate predicate , String name , SWTImageClosureModel model ) {
		model.setFilter(name,predicate.x,predicate.y,predicate.tolerance );
	}	

	def getFormulas( Model m ) {
		m.elements.filter(typeof(SpaceFormulaDeclaration)).map[it.name]
	}
	
	def getLabels( Model m ) {
		m.elements.filter(typeof(LabelDeclaration)).map[it.name]
	}
	
	def getPixelsWithLabel( SWTImageClosureModel model , String label ) {
		model.getSet(label);
	}
	
	def getNodesWithLabel( GraphClosureModel model , String label ) {
		model.getSet(label);
	}
	
	def getFormula( Model m , String name ) {
		var f = m.elements.filter(typeof(SpaceFormulaDeclaration)).findFirst[it.name == name]
		if (f != null) {
			var generator = new SlcsFormulaGenerator();
			generator.createFormula(f);
	 	} else {
	 		null
	 	}
	}
	
	def modelCheckingGraphModel( GraphClosureModel model , SLCSFormula f ) {
		var mc = new SLCSModelChecker<Node>( model );
		return mc.sat( f )
	} 
	
	def modelCheckingImageModel( SWTImageClosureModel model , SLCSFormula f ) {
		var mc = new SLCSModelChecker<Pixel>( model );
		return mc.sat( f )
	} 

	

}