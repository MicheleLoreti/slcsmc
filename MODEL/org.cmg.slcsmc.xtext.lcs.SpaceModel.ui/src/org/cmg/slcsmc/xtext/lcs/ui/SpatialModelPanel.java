/**
 * 
 */
package org.cmg.slcsmc.xtext.lcs.ui;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import org.cmg.slcsmc.model.ClosureModel;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;

/**
 * @author loreti
 *
 */
public abstract class SpatialModelPanel<S> {

	public ClosureModel<S> model;
	private LinkedList<String> layers;
	private HashMap<String, Set<S>> elements;
	private HashMap<String, Boolean> visible;
	private HashMap<String, Color> colors;
	
	public SpatialModelPanel( ClosureModel<S> model ) {
		this.model = model;
		this.elements = new HashMap<String,Set<S>>();
		this.layers = new LinkedList<>();
		this.visible= new HashMap<String,Boolean>();
	}
	
	public abstract void buildModelPanel( Composite parent );

	protected void isChanged() {		
	}
	
	public void setPoint( String name , Set<S> set ) {
		Object old = this.elements.put(name, set);
		if (old != null) {
			this.layers.remove( this.layers.indexOf(name) );
		}
		this.visible.put(name, true);
		this.layers.add(name);
	}
	
	public void setColor( String name , Color c ) {
		this.colors.put(name, c);
	}
	
	public Color getColor( String name ) {
		return this.colors.get(name);
	}
	
	public boolean isVisible( String name ) {
		Boolean v = this.visible.get(name);
		if (v == null) {
			v = false;
		}
		return v;
	}
	
	public void moveUp( int idx ) {
		if ((idx>0)&&(idx<layers.size())) {
			String current = layers.get(idx);
			String previous = layers.get(idx-1);
			layers.set(idx-1, current);
			layers.set(idx, previous);
		}
	}
	
	public void moveDown( int idx ) {
		if ((idx>=0)&&(idx<(layers.size()-1))) {
			String current = layers.get(idx);
			String previous = layers.get(idx+1);
			layers.set(idx+1, current);
			layers.set(idx, previous);
		}
		
	}

	public void setVisible( String name , boolean v ) {
		Boolean value = this.visible.get(name);
		if (value != null) {
			this.visible.put(name, v);
		}
	}
	

	public void setVisible( boolean v ) {
		for (Entry<String, Boolean> entry : visible.entrySet()) {
			entry.setValue(v);
		}
	}
}
