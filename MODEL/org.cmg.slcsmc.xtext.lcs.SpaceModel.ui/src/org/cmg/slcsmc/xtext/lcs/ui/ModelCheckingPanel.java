/**
 * 
 */
package org.cmg.slcsmc.xtext.lcs.ui;

import java.util.HashMap;

import org.cmg.slcsmc.logic.SLCSFormula;
import org.cmg.slcsmc.logic.mc.SLCSModelChecker;
import org.cmg.slcsmc.model.ClosureModel;
import org.eclipse.jface.viewers.ColorCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * @author loreti
 *
 */
public class ModelCheckingPanel<S> {

	private SpatialModelPanel<S> panel;
	private ClosureModel<S> model;
	private SLCSModelChecker<S> modelChecker;
	private HashMap<String, SLCSFormula> formulas;

	public ModelCheckingPanel( Composite parent , SpatialModelPanel<S> panel , ClosureModel<S> model , HashMap<String,SLCSFormula> formulas ) {
		this.panel = panel;
		this.model = model;
		this.modelChecker = new SLCSModelChecker<S>(model);
		this.formulas = formulas;
		buildPanel( parent );
	}

	private void buildPanel(Composite parent) {
		GridLayout layout = new GridLayout();
		parent.setLayout(layout);
		
	}
	
	private void buildSideBar( Composite parent ) {
		Composite sideBarContainer = new Composite(parent, SWT.NONE);

	}
	
	
	
}
