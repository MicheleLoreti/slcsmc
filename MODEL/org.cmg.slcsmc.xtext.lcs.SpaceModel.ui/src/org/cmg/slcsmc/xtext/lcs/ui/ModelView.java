/**
 * 
 */
package org.cmg.slcsmc.xtext.lcs.ui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cmg.slcsmc.model.graphs.GraphClosureModel;
import org.cmg.slcsmc.model.images.SWTImageClosureModel;
import org.cmg.slcsmc.xtext.lcs.spaceModel.Model;
import org.cmg.slcsmc.xtext.lcs.util.GraphModelGenerator;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.part.ViewPart;

import com.google.inject.Inject;

/**
 * @author loreti
 *
 */
public class ModelView extends ViewPart {

	private Composite container;
	private Combo resourceSelection;
	private Combo modelSelection;
	private @Inject Util util;
	private IResource selectedResource;
	private Model selectedModel;
	private HashMap<IResource, Model> activeModels;
	private IResource[] acviteResources;
	private Model availableModel;
	private List<String> graphModels;
	private List<String> imageModels;
	private CTabFolder modelContainer;

	/**
	 * 
	 */
	public ModelView() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		container = new Composite(parent, SWT.BORDER);
		GridLayout layout = new GridLayout(4, false);
		container.setLayout(layout);
		
		/* BUILD COMBO PANELS */
		Label selectFileLabel = new Label(container, SWT.NONE);
		selectFileLabel.setText("File:");
		GridData data = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		selectFileLabel.setLayoutData(data);
		
		resourceSelection = new Combo(container, SWT.NONE);
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		resourceSelection.setLayoutData(data);
		resourceSelection.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				doSelectFile();
			}
			
		});

		Label selectModelLabel = new Label(container, SWT.NONE);
		selectModelLabel.setText("Model:");
		data = new GridData(SWT.LEFT, SWT.CENTER, false, false);
		selectModelLabel.setLayoutData(data);
		
		modelSelection = new Combo(container, SWT.NONE);
		data = new GridData(SWT.FILL, SWT.CENTER, true, false);
		modelSelection.setLayoutData(data);
		modelSelection.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				doSelectSpatialModel();
			}
			
		});

		modelContainer = new CTabFolder(container, SWT.BORDER);
		data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.horizontalSpan = 4;
		modelContainer.setLayoutData(data);
		
		resetRecomputeData();
		
	}

	protected void doSelectSpatialModel() {
		int idx = modelSelection.getSelectionIndex();
		if (idx < 0) {
			
			clearDepictedModel();
			
		} else {
			if (idx < graphModels.size()) {
				
				showGraphModel( graphModels.get(idx) );
				
			} else {
			
				showImageModel( imageModels.get(idx-graphModels.size()));
				
			}
		}		
	}

	private void showImageModel(String name) {
		try {
			SWTImageClosureModel model = util.getImageModel(selectedResource.getProject(),selectedModel, name);			
			if (model != null) {
				CTabItem item = new CTabItem(modelContainer, SWT.NONE);
				item.setText(name);
				ScrolledComposite composite = new ScrolledComposite(modelContainer, SWT.V_SCROLL|SWT.H_SCROLL);
				composite.setLayout(new FillLayout());
//				Canvas canvas = new Canvas(modelContainer, SWT.NO_REDRAW_RESIZE);
//				Display display = modelContainer.getDisplay();
//				Image toShow = new Image(display, model.getImageData());
//				canvas.addPaintListener(new PaintListener() {
//			        public void paintControl(PaintEvent e) {
//			         e.gc.drawImage(toShow,0,0);
//			        }
//			    });
//				item.setControl(canvas);
				ImagePanel canvas = new ImagePanel(composite, SWT.NO_REDRAW_RESIZE);
				Display display = modelContainer.getDisplay();
				Image toShow = new Image(display, model.getImageData());
				canvas.setData(toShow);
				item.setControl(composite);
				composite.setContent(canvas);
				modelContainer.setSelection(item);
			} else {
				MessageDialog.openInformation(this.getSite().getShell(), "Error!" , "Selected model cannot be opened!");
			}
		} catch (Exception e) {
			MessageDialog.openError(this.getSite().getShell(), "Error..." , e.getMessage());
		}		
	}

	private void showGraphModel(String name) {
		try {
			GraphClosureModel model = util.getGraphModel(selectedModel, name);
			/*
			 * ADD HERE THE CODE TO DISPLAY GraphClosureModel
			 * 
			 */
		} catch (Exception e) {
			MessageDialog.openError(this.getSite().getShell(), "Error..." , e.getMessage());
		}
	}

	private void clearDepictedModel() {
		System.out.println("CLEAR!");
	}

	protected void doSelectFile() {
		int idx = resourceSelection.getSelectionIndex();
		if (idx < 0) {
			selectedResource = null;
			selectedModel = null;
			modelSelection.removeAll();
		} else {
			selectedResource = acviteResources[idx];
			selectedModel = activeModels.get(selectedResource);
			if (selectedModel != null) {
				graphModels = util.getGraphModels(selectedModel);
				imageModels = util.getImageModels(selectedModel);
			} else {
				graphModels = new LinkedList<>();
				imageModels = new LinkedList<>();
			}
			String[] elements = new String[ graphModels.size()+imageModels.size()];
			int count = 0;
			for (String m : graphModels) {
				elements[count] = m;
				count++;
			}
			for (String m : imageModels) {
				elements[count] = m;
				count++;
			}
			modelSelection.setItems(elements);
		}
		container.layout();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		resetRecomputeData();
	}

	private void resetRecomputeData() {
		activeModels = util.getActiveModels();		
		acviteResources = activeModels.keySet().toArray( new IResource[ activeModels.size()] );
		String[] elements = new String[acviteResources.length];
		for (int i=0 ; i<elements.length ; i++) {
			elements[i] = acviteResources[i].getName();
		}
		resourceSelection.setItems(elements);
		container.layout();
	}

}
