/**
 * 
 */
package org.cmg.slcsmc.xtext.lcs.ui;

import java.util.Set;

/**
 * @author loreti
 *
 */
public class ModelCheckingResults<T> {
	
	private String label;
	
	private Set<T> points;

	private boolean isVisible;
	
	public ModelCheckingResults( String label , Set<T> points ) {
		this.label = label;
		this.points = points;
		this.isVisible = true;
	}


	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}


	/**
	 * @return the points
	 */
	public Set<T> getPoints() {
		return points;
	}


	/**
	 * @return the isVisible
	 */
	public boolean isVisible() {
		return isVisible;
	}


	/**
	 * @param isVisible the isVisible to set
	 */
	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
