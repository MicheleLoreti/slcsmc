/**
 * 
 */
package org.cmg.slcsmc.xtext.lcs.ui;

import java.util.ArrayList;
import java.util.Set;

import org.cmg.slcsmc.model.images.Pixel;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

/**
 * @author loreti
 *
 */
public class ImagePanel extends Canvas {

	
	private float zoom = (float) 2.0;
	private Image image;
	private ArrayList<Image> layers = new ArrayList<>();
	private ArrayList<Boolean> mask = new ArrayList<>();
	
	
	//private ArrayList<ModelCheckingResults<Pixel>> layers; 
	
	
	public ImagePanel(Composite parent, int style) {
		super(parent, style);
		addPaintListener(new PaintListener() {
	        public void paintControl(PaintEvent e) {
	        	if (image != null) {
	   	         	e.gc.drawImage(image,0,0);
	        	}
	        }
	    });
	}
	
	public void setData( Image image ) {
		this.image = image;
		this.setSize((int) (image.getImageData().width*zoom)+1, (int) (image.getImageData().height*zoom)+1 );
	}


	protected void paint( GC gc ) {
		Transform transform = new Transform(gc.getDevice());
		gc.getTransform(transform);
		transform.scale(zoom, zoom);
		gc.setTransform(transform);
		transform.dispose();
		if (image != null) {
			gc.drawImage(image, 0, 0);
		}
		for (int i=0 ; i<layers.size() ; i++ ) {
			if (mask.get(i)) {
				gc.drawImage(layers.get(i), 0, 0);
			}
		}
	}
	
	public void addLayer( Set<Pixel> data , RGB color ) {
		PaletteData palette = new PaletteData(0xFF , 0xFF00 , 0xFF0000);
		ImageData layerImageData = new ImageData(image.getImageData().width, image.getImageData().height, 24, palette);		
		int pixelValue = palette.getPixel(color);
		for (Pixel pixel : data) {
			layerImageData.setPixel(pixel.getX(), pixel.getY(), pixelValue);
		}
		layers.add( new Image(getDisplay(), layerImageData));
		mask.add(true);
	}
	
	public void moveUp( int i ) {
		swapLayer(i, i-1);
	}
	
	public void moveDown( int i ) {
		swapLayer(i, i+1);
	}

	public void hide( int i ) {
		mask.set(i, false);
	}
	
	public void show( int i ) {
		mask.set(i, true);
	}
	
	private void swapLayer( int i , int j ) {
		Image elementAti = layers.get(i);
		Image elementAtj = layers.get(j);
		Boolean isVisiblei = mask.get(i);
		Boolean isVisiblej = mask.get(j);
		layers.set(i, elementAtj);
		layers.set(j, elementAti);
		mask.set(i, isVisiblej);
		mask.set(j, isVisiblei);
		
	}
}
