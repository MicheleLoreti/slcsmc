package org.cmg.slcsmc.xtext.lcs.SpaceModel.tests

import org.eclipse.xtext.junit4.InjectWith
import org.cmg.slcsmc.xtext.lcs.SpaceModelInjectorProvider
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.XtextRunner
import com.google.inject.Inject
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.eclipse.xtext.junit4.util.ParseHelper
import org.cmg.slcsmc.xtext.lcs.spaceModel.Model
import org.junit.Test
import org.cmg.slcsmc.xtext.lcs.spaceModel.ConstantDeclaration

@InjectWith(typeof(SpaceModelInjectorProvider))
@RunWith(typeof(XtextRunner))
class TestExpressionEvaluator {
	
	@Inject extension ValidationTestHelper
	
	@Inject extension ParseHelper<Model>
	
	@Test
	def evalConsantExpression() {
		'''
		const test = 2;
		'''.parse => [
			it.elements.filter(typeof(ConstantDeclaration)).filter[ it.name == "test" ]
			assertNoErrors
		]
	}
	
}