package org.cmg.slcsmc.xtext.lcs.SpaceModel.tests

import org.eclipse.xtext.junit4.InjectWith
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.XtextRunner
import org.cmg.slcsmc.xtext.lcs.SpaceModelInjectorProvider
import org.junit.Test
import com.google.inject.Inject
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.eclipse.xtext.junit4.util.ParseHelper
import org.cmg.slcsmc.xtext.lcs.spaceModel.Model
import org.cmg.slcsmc.xtext.lcs.spaceModel.GraphDeclaration
import static extension org.junit.Assert.*
import java.util.ArrayList
import java.util.HashMap
import org.cmg.slcsmc.xtext.lcs.util.GraphModelGenerator

@InjectWith(typeof(SpaceModelInjectorProvider))
@RunWith(typeof(XtextRunner))
class TestGraphGenerator {
	
	@Inject extension ValidationTestHelper
	
	@Inject extension ParseHelper<Model>
	
	@Inject extension GraphModelGenerator
	
	@Test
	def testCombine( ) {
		assertNotNull( combine(#[ 0 , 1 , 2 ] , new ArrayList<ArrayList<Integer>>() ) )
	}
	
	@Test
	def void testOne() {
		'''
		graph Test {
			signature [ 0 .. 10 ];	
		}
		'''.parse => [
			assertNoErrors
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( newHashMap() )
			assertEquals( 11 , m.numberOfVertexes() )
		]
	}

	@Test
	def void testTwo() {
		'''
		graph Test {
			signature [ 0 .. 2 ];	
			edges {
				< 0 > -> < 1 >;
				< 1 > -> < 2 >;
				< 2 > -> < 0 >;
			}
		}
		'''.parse => [
			assertNoErrors
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( newHashMap() )
			assertEquals( 3 , m.numberOfVertexes() )
			assertEquals( 3 , m.numberOfEdges() )
		]
	}

	@Test
	def void testThree() {
		'''
		graph Test {
			signature [ 0 .. 2 ],[ 3 .. 5 ];	
			edges {
				for x in [ 0 .. 2 ] {
					for y in [ 3 .. 5 ] {
						< x , y > -> < (x+1) % 3 , y >; 
					}	
				}	
			}
		}
		'''.parse => [
			assertNoErrors 
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( new HashMap<String,Integer>() )
			assertEquals( 9 , m.numberOfVertexes() )
			assertTrue( m.contains( #[ 0 , 3 ] ) )
			assertFalse( m.contains( #[ 3 , 0 ] ) )
			assertEquals( 9 , m.numberOfEdges() )
			for( var i=0 ; i<= 2 ; i++ ) {
				for( var j=3 ; j<=5 ; j++ ) {
					assertTrue( m.contains( #[ i , j ] , #[ (i+1)%3 , j ] ))		
				}
			}
		]
	}

	@Test
	def void testFour() {
		'''
		graph Test {
			signature [ 0 .. 10 ],[ 0 .. 10 ];	
			edges {
				for x in [ 0 .. 9 ] {
					for y in [ 0 .. 9 ] {
						< x , y > -> < (x+1) , y >; 
						< x , y > -> < x , (y+1) >; 
					}	
				}	
			}
		}
		'''.parse => [
			assertNoErrors 
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( newHashMap() )
			assertEquals( 121 , m.numberOfVertexes() )
			assertEquals( 200 , m.numberOfEdges() )
		]
	}

	@Test
	def void testFive() {
		'''
		label corners;
		
		label diagonal;
		
		graph Test {
			signature [ 0 .. 10 ],[ 0 .. 10 ];	
			edges {
				for x in [ 0 .. 9 ] {
					for y in [ 0 .. 9 ] {
						< x , y > -> < (x+1) , y >; 
						< x , y > -> < x , (y+1) >; 
					}	
				}	
			}
			labels {
				corners = { < 0 , 0 > , < 10 , 10 > , < 10 , 0 > , <0 , 10 > };
				diagonal = [ 0 ] == [ 1 ];
			}
		}
		'''.parse => [
			assertNoErrors 
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( newHashMap() )
			assertEquals( 121 , m.numberOfVertexes() )
			assertEquals( 200 , m.numberOfEdges() )
			assertEquals( 4 ,  m.getSet("corners").size() ) 
			assertEquals( 11 ,  m.getSet("diagonal").size() ) 
		]
	}
	
	@Test
	def void testSix() {
		'''
		parameter N;
		
		label corners;
		
		label diagonal;
		
		graph Test {
			signature [ 0 .. N ],[ 0 .. N ];	
			edges {
				for x in [ 0 .. (N-1) ] {
					for y in [ 0 .. (N-1) ] {
						< x , y > -> < (x+1) , y >; 
						< x , y > -> < x , (y+1) >; 
					}	
				}	
			}
			labels {
				corners = { < 0 , 0 > , < N , N > , < N , 0 > , <0 , N > };
				diagonal = [ 0 ] == [ 1 ];
			}
		}
		'''.parse => [
			var N = 5
			assertNoErrors 
			var gd = it.elements.filter(typeof(GraphDeclaration)).findFirst[it.name=="Test"]
			assertNotNull( gd )
			var m = gd.generateGraphModel( newHashMap("N" -> N) )
			assertEquals( (N+1)*(N+1) , m.numberOfVertexes() )
			assertEquals( 2*(N*N) , m.numberOfEdges() )
			assertEquals( 4 ,  m.getSet("corners").size() ) 
			assertEquals( N+1 ,  m.getSet("diagonal").size() ) 
		]
	}
}