package org.cmg.slcsmc.xtext.lcs.util

import org.cmg.slcsmc.logic.SLCSFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.SpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.IfSpaceFormula
import org.cmg.slcsmc.logic.SLCSOr
import org.cmg.slcsmc.logic.SLCSNot
import org.cmg.slcsmc.xtext.lcs.spaceModel.IfAndOnlyIfCollectiveFormula
import org.cmg.slcsmc.logic.SLCSAnd
import org.cmg.slcsmc.logic.SLCSIf
import org.cmg.slcsmc.logic.SLCSIfAndOnlyIf
import org.cmg.slcsmc.xtext.lcs.spaceModel.OrSpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.AndSpaceFormula
import org.cmg.slcsmc.logic.SLCSDiffusion
import org.cmg.slcsmc.xtext.lcs.spaceModel.DiffusionSpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.SurroundSpaceFormula
import org.cmg.slcsmc.logic.SLCSSurround
import org.cmg.slcsmc.xtext.lcs.spaceModel.ReachabilitySpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.SeparationSpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.TrueExpression
import org.cmg.slcsmc.logic.SLCSTrue
import org.cmg.slcsmc.xtext.lcs.spaceModel.FalseExpression
import org.cmg.slcsmc.logic.SLCSFalse
import org.cmg.slcsmc.xtext.lcs.spaceModel.ClosureSpaceFormula
import org.cmg.slcsmc.logic.SLCSReachability
import org.cmg.slcsmc.logic.SLCSSeparation
import org.cmg.slcsmc.xtext.lcs.spaceModel.InteriorSpaceFormula
import org.cmg.slcsmc.logic.SLCSInterior
import org.cmg.slcsmc.logic.SLCSClosure
import org.cmg.slcsmc.xtext.lcs.spaceModel.BorderSpaceFormula
import org.cmg.slcsmc.logic.SLCSBorder
import org.cmg.slcsmc.xtext.lcs.spaceModel.ExternalBorderSpaceFormula
import org.cmg.slcsmc.logic.SLCSExternalBorder
import org.cmg.slcsmc.logic.SLCSInternalBorder
import org.cmg.slcsmc.xtext.lcs.spaceModel.InternalBorderSpaceFormula
import org.cmg.slcsmc.logic.SLCSGlobally
import org.cmg.slcsmc.xtext.lcs.spaceModel.GloballySpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.PossibleSpaceFormula
import org.cmg.slcsmc.logic.SLCSPossibly
import org.cmg.slcsmc.xtext.lcs.spaceModel.NotSpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.ReferenceableInSpaceFormula
import org.cmg.slcsmc.xtext.lcs.spaceModel.LabelDeclaration
import org.cmg.slcsmc.logic.SLCSAtomicProposition
import org.cmg.slcsmc.xtext.lcs.spaceModel.SpaceFormulaDeclaration

class SlcsFormulaGenerator {
	
	def  dispatch createFormula( SpaceFormula formula ) {
		
	}
	
	def  dispatch SLCSFormula createFormula( IfSpaceFormula formula ) {
		new SLCSIf (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}
	
	def  dispatch SLCSFormula createFormula( IfAndOnlyIfCollectiveFormula formula ) {
		new SLCSIfAndOnlyIf (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}
	
	def  dispatch SLCSFormula createFormula( OrSpaceFormula formula ) {
		new SLCSOr (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}
	
	def  dispatch SLCSFormula createFormula( AndSpaceFormula formula ) {
		new SLCSAnd (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}
	
	def  dispatch SLCSFormula createFormula( DiffusionSpaceFormula formula ) {
		new SLCSDiffusion (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}
	
	def  dispatch SLCSFormula createFormula( SurroundSpaceFormula formula ) {
		new SLCSSurround (
			formula.left.createFormula ,
			formula.right.createFormula
		)
	}

	def  dispatch SLCSFormula createFormula( ReachabilitySpaceFormula formula ) {
		new SLCSReachability (
			formula.left.createFormula ,
			formula.right.createFormula 
		)
	}

	def  dispatch SLCSFormula createFormula( SeparationSpaceFormula formula ) {
		new SLCSSeparation (
			formula.left.createFormula  ,
			formula.right.createFormula 
		)
	}
	
	def  dispatch SLCSFormula createFormula( TrueExpression formula ) {
		new SLCSTrue()
	}
	
	
	def  dispatch SLCSFormula createFormula( FalseExpression formula ) {
		new SLCSFalse()
	}

	def  dispatch SLCSFormula createFormula( ClosureSpaceFormula formula ) {
		new SLCSClosure( formula.arg.createFormula );
	}
	

	def  dispatch SLCSFormula createFormula( InteriorSpaceFormula formula ) {
		new SLCSInterior( formula.arg.createFormula );
	}
	
	def  dispatch SLCSFormula createFormula( BorderSpaceFormula formula ) {
		new SLCSBorder( formula.arg.createFormula );
	}

	def  dispatch SLCSFormula createFormula( ExternalBorderSpaceFormula formula ) {
		new SLCSExternalBorder( formula.arg.createFormula );
	}

	def  dispatch SLCSFormula createFormula( InternalBorderSpaceFormula formula ) {
		new SLCSInternalBorder( formula.arg.createFormula );
	}

	def  dispatch SLCSFormula createFormula( GloballySpaceFormula formula ) {
		new SLCSGlobally( formula.arg.createFormula );
	}

	def  dispatch SLCSFormula createFormula( PossibleSpaceFormula formula ) {
		new SLCSPossibly( formula.arg.createFormula );
	}

	def  dispatch SLCSFormula createFormula( NotSpaceFormula formula ) {
		new SLCSNot( formula.argument.createFormula );
	}
	
	def  dispatch SLCSFormula createFormula( ReferenceableInSpaceFormula reference ) {
		switch reference {
			LabelDeclaration: new SLCSAtomicProposition(reference.name)
			SpaceFormulaDeclaration: reference.formula.createFormula
		}
	}
	
}