/*
 * generated by Xtext
 */
package org.cmg.slcsmc.xtext.lcs.scoping

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 *
 */
class SpaceModelScopeProvider extends org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider {

}
