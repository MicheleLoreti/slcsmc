package org.cmg.slcsmc.xtext.lcs.util

import java.util.ArrayList
import java.util.HashMap
import java.util.List
import org.cmg.slcsmc.model.graphs.GraphClosureModel
import org.cmg.slcsmc.xtext.lcs.spaceModel.DirectedEdge
import org.cmg.slcsmc.xtext.lcs.spaceModel.EdgeCommand
import org.cmg.slcsmc.xtext.lcs.spaceModel.EdgeDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.EnumerationPredicate
import org.cmg.slcsmc.xtext.lcs.spaceModel.ForIteration
import org.cmg.slcsmc.xtext.lcs.spaceModel.GraphDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.IfStatement
import org.cmg.slcsmc.xtext.lcs.spaceModel.NodeLabel
import org.cmg.slcsmc.xtext.lcs.spaceModel.StateVectorDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.UnDirectedEdge

import org.cmg.slcsmc.xtext.lcs.spaceModel.ExpressionPredicate
import org.cmg.slcsmc.model.graphs.Node
import com.google.inject.Inject

class GraphModelGenerator {
	
	@Inject extension LcsExpressionEvaluator
	
	def generateGraphModel( 
		GraphDeclaration g ,
		HashMap<String,Integer> parameters 
	) {
		var isDirected = g.directed		
		val states = g.states.generateStateSpace(parameters)		
		val model = new GraphClosureModel( isDirected )
		states.forEach[ model.addNode( it )]
		g.edges.forEach[ edge | edge.execute( model , parameters ) ]
		g.labels.forEach[ l | l.addLabel( model , parameters ) ]
		model
	}

	def  addLabel( NodeLabel l , GraphClosureModel model , HashMap<String,Integer> parameters ) {
		model.addAll(l.label.name , l.predicate.evalPredicate( model , parameters )  )
	}
	
	def  dispatch evalPredicate( EnumerationPredicate predicate , GraphClosureModel model , HashMap<String,Integer> parameters ) {
		predicate.states.map[ new Node( it.eval(parameters, null ) ) ]
	}

	def  dispatch evalPredicate( ExpressionPredicate predicate , GraphClosureModel model , HashMap<String,Integer> parameters ) {
		model.vertexes.filter[ predicate.guard.evalBoolean( it , parameters , null ) ]
	}
	
	def  boolean execute( EdgeCommand cmd , GraphClosureModel model , HashMap<String,Integer> parameters ) {
		cmd.execute(model,parameters,new HashMap<String,Integer>());
	}
	
	def  dispatch boolean execute( EdgeCommand cmd , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		false 
	}

	def  dispatch boolean execute( ForIteration cmd , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		val name = cmd.iteration.name
		var old = context.get(name)
		cmd.iteration.element.generateListOfValues( parameters , context ).forEach[ v | 
			context.put(name , v )
			cmd.body.forEach[ it.execute(model,parameters,context) ]
		]
		if (old == null) {
			context.remove(name)
		} else {
			context.put( name , old )
		}
		true
	}
	
	def  dispatch boolean execute( IfStatement cmd , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		if (cmd.guard.evalBoolean(null,parameters,context)) {
			cmd.thenCase.forEach[ it.execute( model , parameters , context ) ]
			true
		} else {
			cmd.elseCase.forEach[ it.execute( model , parameters , context ) ]
			true
		}
	}
	
	def  dispatch boolean execute( EdgeDeclaration edge , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		edge.edge.execute(model,parameters,context)
	}
	
	def  dispatch execute( DirectedEdge edge , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		model.addEdge( edge.src.eval( parameters , context ) , edge.trg.eval( parameters , context ))
	}

	def  dispatch execute( UnDirectedEdge edge , GraphClosureModel model , HashMap<String,Integer> parameters , HashMap<String,Integer> context ) {
		var src = edge.src.eval( parameters , context )
		var trg = edge.trg.eval( parameters , context )
		model.addEdge( src , trg )
		if ( model.isDirected ) {
			model.addEdge( trg , src )
		}
	}
	
	def  generateStateSpace( StateVectorDeclaration states , HashMap<String,Integer> parameters ) {
		var elements = states.elements
		val llist = elements.map[ it.generateListOfValues( parameters , new HashMap<String,Integer>() ) ]
		llist.fold( null , [ l , values | values.combine( l ) ] )
	}
	
	def  combine( int[] values , List<ArrayList<Integer>> states ) {
		if ((states == null)||(states.empty)) {
			val newList = new ArrayList<ArrayList<Integer>>()
			values.forEach[ newList.add( newArrayList(  it ) ) ] 		
			newList
		} else {
			states.fold( 
				new ArrayList<ArrayList<Integer>>() , 
				[ l,state | 
					values.forEach[ 
						var s2 = newArrayList()
						s2.addAll(state)
						s2.add( it )
						l.add( s2 )
					] 
					l										
				]
			)
		}
	}
}