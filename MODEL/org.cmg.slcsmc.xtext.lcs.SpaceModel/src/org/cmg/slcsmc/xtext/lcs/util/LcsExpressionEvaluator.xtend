package org.cmg.slcsmc.xtext.lcs.util

import java.util.HashMap
import org.cmg.slcsmc.model.graphs.Node
import org.cmg.slcsmc.xtext.lcs.spaceModel.AndExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.ConditionalExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.ConstantDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.DisEqualExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.DivExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.EqualExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.Expression
import org.cmg.slcsmc.xtext.lcs.spaceModel.FalseExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.GreaterExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.GreaterOrEqualExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.IfAndOnlyIfExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.IfExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.Interval
import org.cmg.slcsmc.xtext.lcs.spaceModel.Iteration
import org.cmg.slcsmc.xtext.lcs.spaceModel.LessExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.LessOrEqualExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.LitteralExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.MinusExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.ModuloExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.MulExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.NegationExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.OrExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.ParameterDeclaration
import org.cmg.slcsmc.xtext.lcs.spaceModel.PlusExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.PositiveExpression
import org.cmg.slcsmc.xtext.lcs.spaceModel.Reference
import org.cmg.slcsmc.xtext.lcs.spaceModel.Set
import org.cmg.slcsmc.xtext.lcs.spaceModel.StateElement
import org.cmg.slcsmc.xtext.lcs.spaceModel.StateVector
import org.cmg.slcsmc.xtext.lcs.spaceModel.TrueExpression

class LcsExpressionEvaluator {
	
	def  int evalInteger( Expression e , HashMap<String,Integer> parameters ) {
		e.evalInteger( null , parameters , new HashMap<String,Integer>() )
	}
	
	def  dispatch int evalInteger( ConditionalExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		if (e.guard.evalBoolean(state,parameters,values)!=0) {
			e.thenCase.evalInteger(state,parameters,values)
		} else {
			e.elseCase.evalInteger(state,parameters,values)
		}
	}
	
//	def  dispatch int evalInteger( OrExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( IfExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//
//	def  dispatch int evalInteger( IfAndOnlyIfExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//
//	def  dispatch int evalInteger( AndExpression e , Node state, HashMap<String,Integer> parameters ,   HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( LessExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( LessOrEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( EqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( DisEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( GreaterOrEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( GreaterExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( TrueExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
//	
//	def  dispatch int evalInteger( FalseExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		0		
//	}
	
	def  dispatch int evalInteger( PlusExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values)+e.right.evalInteger(state,parameters,values)	
	}
	
	def  dispatch int evalInteger( MinusExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values)-e.right.evalInteger(state,parameters,values)	
	}
	
//	def  dispatch int evalInteger( NotExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
//		e.left.evalInteger(state,parameters,values)+e.right.evalInteger(state,parameters,values)	
//	}

	def  dispatch int evalInteger( MulExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values)*e.right.evalInteger(state,parameters,values)	
	}
	
	def  dispatch int evalInteger( DivExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values)/e.right.evalInteger(state,parameters,values)	
	}
	
	def  dispatch int evalInteger( ModuloExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values)%e.right.evalInteger(state,parameters,values)	
	}
	
	def  dispatch int evalInteger( LitteralExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.value		
	}
	
	def  dispatch int evalInteger( StateElement e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		state.get(e.index.evalInteger(state,parameters,values))		
	}

	def  dispatch int evalInteger( NegationExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		-e.argument.evalInteger(state,parameters,values)		
	}
	
	def  dispatch int evalInteger( PositiveExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		-e.argument.evalInteger(state,parameters,values)		
	}
	
	def  dispatch int evalInteger( Reference e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		val r = e.reference
		switch r {
			ConstantDeclaration: r.value.evalInteger(state,parameters,values)
			ParameterDeclaration: parameters.get(r.name) ?: 0
			Iteration: values.get(r.name) ?: 0
			default:
				0
		}
	}
	
	def  dispatch boolean evalBoolean( ConditionalExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		if (e.guard.evalBoolean(state,parameters,values)!=0) {
			e.thenCase.evalBoolean(state,parameters,values)
		} else {
			e.elseCase.evalBoolean(state,parameters,values)
		}
	}
	
	def  dispatch boolean evalBoolean( OrExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalBoolean(state,parameters,values) || e.right.evalBoolean(state,parameters,values)
	}
	
	def  dispatch boolean evalBoolean( IfExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		( !e.left.evalBoolean(state,parameters,values) ) || e.right.evalBoolean(state,parameters,values)
	}

	def  dispatch boolean evalBoolean( IfAndOnlyIfExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalBoolean(state,parameters,values) || e.right.evalBoolean(state,parameters,values)
	}

	def  dispatch boolean evalBoolean( AndExpression e , Node state, HashMap<String,Integer> parameters ,   HashMap<String,Integer> values ) {
		e.left.evalBoolean(state,parameters,values) && e.right.evalBoolean(state,parameters,values)
	}
	
	def  dispatch boolean evalBoolean( LessExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) < e.right.evalInteger(state,parameters,values)		
	}
	
	
	def  dispatch boolean evalBoolean( LessOrEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) <= e.right.evalInteger(state,parameters,values)		
	}
	
	def  dispatch boolean evalBoolean( EqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) == e.right.evalInteger(state,parameters,values)		
	}
	
	def  dispatch boolean evalBoolean( DisEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) != e.right.evalInteger(state,parameters,values)		
	}
	
	def  dispatch boolean evalBoolean( GreaterOrEqualExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) >= e.right.evalInteger(state,parameters,values)		
	}
	
	def  dispatch boolean evalBoolean( GreaterExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		e.left.evalInteger(state,parameters,values) > e.right.evalInteger(state,parameters,values)		
	}
	
	def  dispatch boolean evalBoolean( TrueExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		true		
	}
	
	def  dispatch boolean evalBoolean( FalseExpression e , Node state, HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		false		
	}	
	
	def  int[] eval( StateVector vector , HashMap<String,Integer> parameters ,  HashMap<String,Integer> values ) {
		vector.elements.map[ it.evalInteger(null,parameters,values) ]	
	}
	
	def  dispatch int[] generateListOfValues( Interval interval , HashMap<String,Integer> parameters , HashMap<String,Integer> values ) {
		val v1 = interval.lowerBound.evalInteger( null , parameters , values )
		val v2 = interval.upperBound.evalInteger( null , parameters , values )
		val lst = newIntArrayOfSize((v2-v1)+1)
		for( var i=0 ; i<(v2-v1)+1 ; i++ ) {
			lst.set( i , v1+i )
		}
		lst
	}
	
	def  dispatch int[] generateListOfValues( Set set , HashMap<String,Integer> parameters , HashMap<String,Integer> values ) {		
		 set.values.map[ e | e.evalInteger(parameters) ]
	}
}