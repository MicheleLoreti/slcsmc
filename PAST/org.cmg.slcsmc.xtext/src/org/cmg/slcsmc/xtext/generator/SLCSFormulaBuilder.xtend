package org.cmg.slcsmc.xtext.generator

import java.awt.Color
import java.util.HashMap
import org.cmg.slcsmc.logic.SLCSAnd
import org.cmg.slcsmc.logic.SLCSAtomicProposition
import org.cmg.slcsmc.logic.SLCSFalse
import org.cmg.slcsmc.logic.SLCSFormula
import org.cmg.slcsmc.logic.SLCSNot
import org.cmg.slcsmc.logic.SLCSOr
import org.cmg.slcsmc.logic.SLCSTrue
import org.cmg.slcsmc.xtext.sLScript.AndFormula
import org.cmg.slcsmc.xtext.sLScript.ClosureFormula
import org.cmg.slcsmc.xtext.sLScript.FalseFormula
import org.cmg.slcsmc.xtext.sLScript.Formula
import org.cmg.slcsmc.xtext.sLScript.IntCodeColour
import org.cmg.slcsmc.xtext.sLScript.Model
import org.cmg.slcsmc.xtext.sLScript.NotFormula
import org.cmg.slcsmc.xtext.sLScript.OrFormula
import org.cmg.slcsmc.xtext.sLScript.Reference
import org.cmg.slcsmc.xtext.sLScript.RgbColour
import org.cmg.slcsmc.xtext.sLScript.SetCommand
import org.cmg.slcsmc.xtext.sLScript.SurroundFormula
import org.cmg.slcsmc.xtext.sLScript.TrueFormula
import org.cmg.slcsmc.xtext.sLScript.MacroDeclaration
import org.eclipse.emf.common.util.EList
import org.cmg.slcsmc.xtext.sLScript.Parameter
import org.cmg.slcsmc.logic.SLCSSurround
import org.cmg.slcsmc.logic.SLCSDiffusion

class SLCSFormulaBuilder {

	def buildFormulae( Model model ) {
		model.commands.filter(typeof(SetCommand)).fold( 
			new HashMap<String,SLCSFormula>() , 
			[ h , c | 
				h.put( c.name , c.formula.buildFormula( h , new HashMap<String,SLCSFormula>() ) )
				h
			]
		)		
	}

	def dispatch SLCSFormula buildFormula( Formula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSFalse( )
	}
	
	def dispatch SLCSFormula buildFormula( OrFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSOr( formula.left.buildFormula(table,context) , formula.right.buildFormula(table,context))
	}
	
	def dispatch SLCSFormula buildFormula( AndFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSAnd( formula.left.buildFormula(table,context) , formula.right.buildFormula(table,context))
	}

	def dispatch SLCSFormula buildFormula( TrueFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSTrue()
	}

	def dispatch SLCSFormula buildFormula( FalseFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSFalse()
	}
	
	def dispatch SLCSFormula buildFormula( NotFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSNot( formula.argument.buildFormula(table,context))
	}
	
	def dispatch SLCSFormula buildFormula( ClosureFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSDiffusion( formula.argument.buildFormula(table,context) , new SLCSFalse() )
	}
	
	def dispatch SLCSFormula buildFormula( SurroundFormula formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSSurround( formula.left.buildFormula(table,context) , formula.right.buildFormula(table,context))
	}
	
	def dispatch SLCSFormula buildFormula( Reference formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		var ref = formula.ref
		switch ref {
			SetCommand: {
				var f = table.get(formula.ref.name)
				if (f == null) {
					new SLCSFalse()
				} else {
					f
				}				
			}
			Parameter: {
				var f = context.get(formula.ref.name)
				if (f == null) {
					new SLCSFalse()
				} else {
					f
				}				
			}
			MacroDeclaration: {
				val newContext = new HashMap<String,SLCSFormula>();
				ref.params.forEach[f,i|newContext.put(f.name,formula.arguments.get(i).buildFormula(table,context))]				
				ref.formula.buildFormula( table , newContext )	
			}
		}
	}
	
	def dispatch SLCSFormula buildFormula( IntCodeColour formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSAtomicProposition(formula.value+"")
	}

	def dispatch SLCSFormula buildFormula( RgbColour	 formula , HashMap<String,SLCSFormula> table , HashMap<String,SLCSFormula> context ) {
		new SLCSAtomicProposition( formula.red+":"+formula.green+":"+formula.blue )
	}
	
//	def  colourCode( SlcsColourConstant contant ) {
//		switch contant {
//		BlackColour:  Color.BLACK.RGB
//		WhiteColour:	Color.WHITE.RGB 
////		| SilverColour: Color.
////		| GrayColour: Color.
////		| RedColour 
////		| MaroonColour 
////	| YellowColour 
////	| OliveColour 
////	| LimeColour 
////	| GreenColour 
////	| AquaColour 
////	| TealColour 
////	| BlueColour 
////	| NavyColour 
////	| FuchsiaColour 
////	| PurpleColour
////	| OrangeColour
////			
//		}
//		
//		
//	}


}