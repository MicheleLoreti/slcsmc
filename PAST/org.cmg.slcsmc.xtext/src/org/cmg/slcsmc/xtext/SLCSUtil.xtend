package org.cmg.slcsmc.xtext

import org.cmg.slcsmc.xtext.sLScript.Model
import org.cmg.slcsmc.xtext.sLScript.SetCommand
import org.cmg.slcsmc.logic.SLCSFormula
import org.cmg.slcsmc.model.images.Pixel
import org.cmg.slcsmc.xtext.generator.SLCSFormulaBuilder

class SLCSUtil {
	
	def String[] getFormula( Model model ) {
		var list = model.commands.filter(typeof(SetCommand)).map[ c | c.name ]
		list
	}

	def SLCSFormula getFormula( Model model , String name ) {
		var builder = new SLCSFormulaBuilder()
		var map = builder.buildFormulae(model)
		map.get(name)
	}	
}