/**
 * 
 */
package org.cmg.slcsmc.model.images;

import java.util.HashSet;
import java.util.Set;

import org.cmg.slcsmc.model.AbstractClosureModel;
import org.cmg.slcsmc.model.ClosureModel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;


/**
 * @author loreti
 *
 */
public class SWTImageClosureModel extends AbstractClosureModel<Pixel>{
	
	/**
	 * 
	 * Added:
	 * setImage(BufferedImage img)
	 */
	protected int tollerance = 100;
	
	private static final int[][] DEFAULT_CLOSURE_MASK_ARRAY = new int[][] {
		new int[] { 0 , -1 } ,
		new int[] { -1 , 0 } ,
		new int[] { 1 , 0 } ,
		new int[] { 0 , 1 }
	};
	
	private static final int[][] EXTENDED_CLOSURE_MASK_ARRAY = new int[][] {
		new int[] { -1 , -1 } ,
		new int[] { -1 , 0 } ,
		new int[] { -1 , 1 } ,
		new int[] { 0 , -1 } ,
		new int[] { 0 , 1 } ,
		new int[] { 1 , -1 } ,
		new int[] { 1 , 0 } ,
		new int[] { 1 , 1 } 
	};
	
	public static final int DEFAULT_CLOSURE_MASK = 0;
	
	public static final int EXTENDED_CLOSURE_MASK = 1;
	
	
	protected int[][] closureMask ;
	
	protected Image image;

	private ImageData imageData;
	
	public SWTImageClosureModel( Image image ) {
		this(image,DEFAULT_CLOSURE_MASK);
	}
	
	public  SWTImageClosureModel( Image image , int closureMask ) {
		this.image = image;
		this.imageData = image.getImageData();
		initClosureMask(closureMask);
	}
	
	
	private void initClosureMask(int closureMask) {
		if (closureMask==EXTENDED_CLOSURE_MASK) {
			this.closureMask = EXTENDED_CLOSURE_MASK_ARRAY;
			return ;
		}
		this.closureMask = DEFAULT_CLOSURE_MASK_ARRAY;
		
	}

	@Override
	public Set<Pixel> getPoints() {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for( int i=0 ; i<imageData.width ; i++ ) {
			for( int j = 0 ; j<imageData.height ; j++ ) {
				toReturn.add( new Pixel( i , j ) );
			}
		}
		return toReturn;
	}

	@Override
	public Set<Pixel> closure(Set<Pixel> set) {
		return closure( set , null );
	}

	private Set<Pixel> closure(Set<Pixel> set, Set<Pixel> avoid) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>( );
		for (Pixel pixel : set) {
			if ((avoid == null)||(!avoid.contains(pixel))) {
				toReturn.add(pixel);
			}
			for (Pixel q :  post( pixel ) ) {
				if ((avoid == null)||(!avoid.contains(q))) {
					toReturn.add(q);
				}
			}
		}
		return toReturn;
	}

	private Pixel shift(Pixel pixel, int[] shift) {
		int x = pixel.getX()+shift[0];
		int y = pixel.getY()+shift[1];
		if ((x<0)||(x>=imageData.width)||(y<0)||(y>=imageData.height)) {
			return null;
		}
		return new Pixel(x,y);
	}

	@Override
	public Set<Pixel> pre(Pixel s) {
		return post( s );
	}

	@Override
	public Set<Pixel> post(Pixel s) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for (int[] shift : closureMask) {
			Pixel foo = shift( s , shift );
			if (foo !=null ) {
				toReturn.add( foo );
			}
		}
		return toReturn;
	}

	@Override
	public Set<Pixel> pre(Set<Pixel> set) {
		return post( set );
	}

	@Override
	public Set<Pixel> post(Set<Pixel> set) {
		return closure( set );
	}

	@Override
	public Set<Pixel> getSet(String name) {
		try {
			String[] codes = name.split(":");
			if (codes.length != 3) {
				return new HashSet<Pixel>();
			}
			ImageData data = imageData;
			RGB color = new RGB( Integer.parseInt(codes[0]),Integer.parseInt(codes[1]),Integer.parseInt(codes[2]));
			HashSet<Pixel> toReturn = new HashSet<Pixel>();
			for( int i=0 ; i<data.width ; i++ ) {
				for( int j = 0 ; j<data.height ; j++ ) {
					RGB pixelColor = data.palette.getRGB(data.getPixel(i, j));
					int dBlue = Math.abs( color.blue-pixelColor.blue );
					int dRed= Math.abs( color.red-pixelColor.red );
					int dGreen = Math.abs( color.green-pixelColor.green );
					if ((dBlue<tollerance)&&(dRed<tollerance)&&(dGreen<tollerance)) {
						toReturn.add( new Pixel( i , j ) );
					}
				}
			}
			return toReturn;
		} catch (NumberFormatException e) {
			return new HashSet<Pixel>();
		}
 	}

	public int getTollerance() {
		return tollerance;
	}

	public void setTollerance(int tollerance) {
		this.tollerance = tollerance;
	}

	public Image getImage() {
		return image;
	}

	/**
	 * This setter method is added for the zoom
	 * 
	 * @param img
	 */
	public void setImage(Image img){
		image=img;
		imageData = image.getImageData();
	}
		
	
}
