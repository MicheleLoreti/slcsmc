/**
 * 
 */
package org.cmg.slcsmc.plugin;

import org.cmg.slcsmc.logic.SLCSFormula;
import org.cmg.slcsmc.logic.mc.SLCSModelChecker;
import org.cmg.slcsmc.model.images.Pixel;
import org.cmg.slcsmc.xtext.SLCSUtil;
import org.cmg.slcsmc.xtext.sLScript.Model;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

/**
 * @author loreti
 *
 */
public class ImageView extends ViewPart {

	//private Label imageContainer;
	public SWTImageCanvas imageCanvas;
	private Label label5;
	ToolBar bar;
	private IPartListener2 pl;
	private Model model;
	private Combo formula;
	private String[] formulae;
	protected RGB selectedColor;
	
	public ImageView() {
		addListener();
	}


	@Override
	public void createPartControl(final Composite parent) {			
        
		GridLayout gridLayout = new GridLayout();
		GridData data; 

		
		gridLayout.numColumns = 1;
		gridLayout.makeColumnsEqualWidth = true;
		
        parent.setLayout(gridLayout);

        bar = new ToolBar (parent, SWT.FLAT);
		ToolItem load = new ToolItem (bar, SWT.PUSH);
		load.setText ("Load");
		ToolItem zoomIn = new ToolItem (bar, SWT.PUSH);
		zoomIn.setText("ZoomIn");
		ToolItem zoomOut= new ToolItem(bar, SWT.PUSH);
		zoomOut.setText("ZoomOut");
		ToolItem fit= new ToolItem(bar, SWT.PUSH);
		fit.setText("Fit");
		ToolItem rotate= new ToolItem(bar, SWT.PUSH);
		rotate.setText("Rotate");
		ToolItem original= new ToolItem(bar, SWT.PUSH);
		original.setText("Original");
		
		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		bar.setLayoutData(data);
		
		createModelCheckerPanel(parent);
		
		imageCanvas=new SWTImageCanvas(parent);
				
	    Listener toolbarListener = new Listener() {
	        public void handleEvent(Event event) {
	          ToolItem toolItem = (ToolItem) event.widget;
	          String caption = toolItem.getText();
	          do_run(caption,parent);
	        }
	    };
	    
	    load.addListener(SWT.Selection, toolbarListener);
	    zoomIn.addListener(SWT.Selection, toolbarListener);
	    zoomOut.addListener(SWT.Selection, toolbarListener);
	    fit.addListener(SWT.Selection, toolbarListener);
	    rotate.addListener(SWT.Selection, toolbarListener);
	    original.addListener(SWT.Selection, toolbarListener);
		
		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		data.verticalAlignment = GridData.FILL;
		data.grabExcessVerticalSpace = true;

	    imageCanvas.setLayoutData(data);
	    

	    
	}


	private void createModelCheckerPanel(final Composite parent) {
		Composite modelCheckerPanel = new  Composite(parent, SWT.FLAT);
		
		GridLayout layout = new GridLayout();
		layout.numColumns = 7;
		
		modelCheckerPanel.setLayout(layout);
		
		Label label2 = new Label(modelCheckerPanel, SWT.LEFT);
		label2.setText("Formula:");
		formula = new Combo (modelCheckerPanel, SWT.READ_ONLY);
		formula.setItems (new String [] { });
		Label label3 = new Label(modelCheckerPanel, SWT.LEFT);
		label3.setText("Color:");
		final Label label4 = new Label(modelCheckerPanel, SWT.LEFT);
		label4.setText("          ");
		this.selectedColor = new RGB(255, 0, 0);
		label4.setBackground(new Color(parent.getDisplay(),this.selectedColor));
		Button select = new Button(modelCheckerPanel, SWT.PUSH);
		select.setText("Color");
		select.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				ColorDialog cd = new ColorDialog(parent.getShell() );
				RGB rgb = cd.open();
				if (rgb != null) {
					ImageView.this.selectedColor = rgb;
					label4.setBackground(new Color(parent.getDisplay(),rgb));
				}
				
			}
		});
		
		Button mcb = new Button(modelCheckerPanel, SWT.PUSH);
		mcb.setText("Show");
		mcb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				SLCSUtil util = new SLCSUtil();
				int idx = formula.getSelectionIndex();
				if (idx >= 0) {
					System.out.println("Selected: "+formulae[idx]+" Color: "+selectedColor);
					SLCSFormula formula = util.getFormula(model, formulae[idx]); 
					if (formula != null) {
						imageCanvas.addLayer(selectedColor, formula);
					}
				}
			}
		});

		Button clearb = new Button(modelCheckerPanel, SWT.PUSH);
		clearb.setText("Clear");
		clearb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				imageCanvas.clearLayers();
				
			}
		});

		label5 = new Label(modelCheckerPanel, SWT.LEFT);
		label5.setText("255:255:255");
		label5.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		
		GridData data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		modelCheckerPanel.setLayoutData(data);

		
		
	}


	public void do_run(String id,final Composite parent) {
		
		if(id.equals("Load")){
			imageCanvas.onFileOpen();
			imageCanvas.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseUp(MouseEvent e) {					
				}
				@Override
				public void mouseDown(MouseEvent e) {
					ImageData data = imageCanvas.getScreenImage().getImageData();
					int pixelValue = data.getPixel(e.x, e.y);					
					System.out.println("Pixel: "+pixelValue+" X: "+e.x+" Y: "+e.y);
					int a = (pixelValue)&0xFF;
					int r = (pixelValue>>8)&0xFF;
					int g = (pixelValue>>16)&0xFF;
					int b = (pixelValue>>24)&0xFF;
					System.out.println(r+"\n"+g+"\n"+b+"\n"+a);
//					setLabel5Color(new RGB(r, g, a),parent);
					
					setLabel5Color(data.palette.getRGB(pixelValue),parent);
				}
				
				@Override
				public void mouseDoubleClick(MouseEvent e) {					
					imageCanvas.zoomIn();
				}
				
			});
			return;
		}
		
		if(imageCanvas.getSourceImage()==null) return;
		
		if(id.equals("ZoomIn")){
			imageCanvas.zoomIn();
			return;
		}
		else if(id.equals("ZoomOut"))
		{
			imageCanvas.zoomOut();
			return;
		}
		else if(id.equals("Fit"))
		{
			imageCanvas.fitCanvas();
			return;
		}
		else if(id.equals("Rotate"))
		{
			ImageData src=imageCanvas.getImageData();
			if(src==null) return;
			PaletteData srcPal=src.palette;
			PaletteData destPal;
			ImageData dest;
			if(srcPal.isDirect){
				destPal=new PaletteData(srcPal.redMask,srcPal.greenMask,srcPal.blueMask);
			}else{
				destPal=new PaletteData(srcPal.getRGBs());
			}
			dest=new ImageData(src.height,src.width,src.depth,destPal);
			for(int i=0;i<src.width;i++){
				for(int j=0;j<src.height;j++){
					int pixel=src.getPixel(i,j);
					dest.setPixel(j,src.width-1-i,pixel);
				}
			}
			imageCanvas.setImageData(dest);
			return;
		}
		else if(id.equals("Original"))
		{
			imageCanvas.showOriginal();
			return;
		}
	}


	protected void setLabel5Color(RGB rgb,Composite parent) {
		label5.setBackground(new Color(parent.getDisplay(),rgb));
		label5.setText(rgb.red+":"+rgb.green+":"+rgb.blue);
	}
	
	@Override
	public void setFocus() {
		imageCanvas.setFocus();	
	}
	
	private void addListener() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		final IWorkbenchPage page = window.getActivePage();
		pl = new IPartListener2() {			   
			   
			   @Override
			   public void partVisible(IWorkbenchPartReference partRef) {
				   System.out.println("VISIBLE: "+partRef.getTitle());
			   }
			   
			   @Override
			   public void partOpened(IWorkbenchPartReference partRef) {
			    // TODO Auto-generated method stub
			    
			   }
			   
			   @Override
			   public void partInputChanged(IWorkbenchPartReference partRef) {

			   }
			   
			   @Override
			   public void partHidden(IWorkbenchPartReference partRef) {
			    // TODO Auto-generated method stub
			    
			   }
			   
			   @Override
			   public void partDeactivated(IWorkbenchPartReference partRef) {
				   System.out.println("DEACTIVATED: "+partRef.getTitle());
			   }
			   
			   @Override
			   public void partClosed(IWorkbenchPartReference partRef) {
			    // TODO Auto-generated method stub
			    
			   }
			   
			   @Override
			   public void partBroughtToTop(IWorkbenchPartReference partRef) {
			    // TODO Auto-generated method stub
			    
			   }
			   
			   @Override
			   public void partActivated(IWorkbenchPartReference partRef) {
//			    try {
				   /*
				    * Modifica Ercoli
				    * 
				    * 
				    */
					   IEditorPart editor1 = page.getActiveEditor();
					   if(editor1 instanceof XtextEditor){
						   XtextEditor xEditor = (XtextEditor) editor1;
						   EList<EObject> values = xEditor.getDocument().readOnly(new IUnitOfWork<EList<EObject>, XtextResource>(){
						        
	                           @Override
	                           public EList<EObject> exec(XtextResource state) throws Exception {
	                                   if (state.getErrors().size()>0) {
	                                           return null;
	                                   }
	                                   return state.getContents();
	                           }
	                           
	                   }); 
					   EObject model = null;
	                   if ((values != null)&&(values.size() > 0)) {
	                           model =  values.get(0);
	                   }					   
                   	   
	                   if (model instanceof Model) {
	                	   System.out.println("EUREKA!!!!");
	                	   setModel( (Model) model );
	                	   
	                   }
	                   
//					   if(model != null){
//						   //System.out.println("ACTIVATED EC_VIEW: "+editor.getTitle());
//						   IFile file = (IFile) editor.getResource();
//						   String extension = file.getFileExtension();
//						   TAPAsElementViewProvider provider = TAPAsProjectHelper.getElementViewProvider(extension);
//						   if (provider != null) {
//							   //EObject model = TAPAsProjectHelper.getModel(editor);
//							   provider.setModel( model );
//						   }
//						   setElementViewProvider(provider);
//					   }
				   }
				   
				   /*
				    * Loreti
				    */
//			     if(partRef.getPart(false) instanceof XtextEditor){
//			      XtextEditor editor = (XtextEditor) partRef.getPart(false);
//			      System.out.println("ACTIVATED: "+editor.getTitle());
//					IFile file = (IFile) editor.getResource();
//					String extension = file.getFileExtension();
//					TAPAsElementViewProvider provider = TAPAsProjectHelper.getElementViewProvider(extension);
//					if (provider != null) {
//						EObject model = TAPAsProjectHelper.getModel(editor);
//						provider.setModel( model );
//					}
//					setElementViewProvider(provider);
////			      IViewPart view = page.showView(TAPAsElementView.ID);
////			      if (view instanceof TAPAsElementView) {
////			    	  ((TAPAsElementView) view).setElementViewProvider(provider);
////			      }
//			     }
////			    } catch (PartInitException e) {
////			     // TODO Auto-generated catch block
////			     e.printStackTrace();
////			    }
			    
			   }
			  };
			  page.addPartListener(pl);				  
	}


	protected void setModel(Model model) {
		this.model = model;
		updateElement();
	}


	private void updateElement() {
		SLCSUtil util = new SLCSUtil();
		if (model != null) {
			this.formulae = util.getFormula(model);
			this.formula.setItems(formulae);
		}
	}
	

}
