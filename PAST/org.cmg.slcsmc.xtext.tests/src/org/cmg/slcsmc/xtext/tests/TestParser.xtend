package org.cmg.slcsmc.xtext.tests

import com.google.inject.Inject
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.eclipse.xtext.junit4.util.ParseHelper
import org.cmg.slcsmc.xtext.sLScript.Model
import org.junit.Test
import org.cmg.slcsmc.xtext.SLScriptInjectorProvider
import org.eclipse.xtext.junit4.InjectWith
import org.junit.runner.RunWith
import org.eclipse.xtext.junit4.XtextRunner

@InjectWith(typeof(SLScriptInjectorProvider))
@RunWith(typeof(XtextRunner))
class TestParser {
	
    @Inject extension ValidationTestHelper
        
    @Inject extension ParseHelper<Model>
	

	
	@Test
	def testSetAndUse() {
		'''
		B = rgb(255,0,0);
		A = rgb(255,255,255);
		C = A and B;		
		'''.parse.assertNoErrors
	}

	
	@Test
	def testMazeExit() {
		'''
		white = rgb(255,255,255);
		green = rgb(0,255,0);
		blue = rgb(0,0,255);
		
		let reach( a , b ) = ![ !b ]\S[ !a ];
		let reachThrough( a , b ) = a & reach( ( a | b),b);
		
		toExit = reachThrough(white,green);
		fromStartToExit = toExit & reachThrough(white,blue);
		startCanExit = reachThrough(blue,fromStartToExit);		
		'''.parse.assertNoErrors
	}
	
	
}