/**
 * 
 */
package org.cmg.slcsmc.model;

import javax.swing.JFrame;

/**
 * @author Alessio
 *
 */
public interface InterfaceModel {
	
	public JFrame getFrame();
	
}
