/**
 * 
 */
package org.cmg.slcsmc.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author loreti
 *
 */
public abstract class AbstractClosureModel<S> implements ClosureModel<S>{

	@Override
	public final Set<S> getExternalBorder(Set<S> set) {
		Set<S> toReturn = closure(set);
		toReturn.removeAll(set);
 		return toReturn;
	}

	@Override
	public final Set<S> getInternalBorder(Set<S> set) {
		Set<S> toReturn = new HashSet<S>( );
		Set<S> interior = getInterior(set);
		for (S n : set) {
			if (!interior.contains(n)) {
				toReturn.add(n);
			}
		}
		return toReturn;
	}

	@Override
	public final Set<S> getInterior(Set<S> set) {
		Set<S> foo = getPoints();
		foo.removeAll(set);
		foo = closure( foo );
		Set<S> interior = getPoints();
		interior.removeAll(foo);
 		return interior;
	}

	@Override
	public final Set<S> getBorder(Set<S> set) {
		Set<S> closure = closure( set );
		closure.removeAll(getInterior(set));
		return closure;
	}

}
