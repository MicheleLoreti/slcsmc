/**
 * 
 */
package org.cmg.slcsmc.model;

import java.util.Set;

/**
 * @author loreti
 *
 */
public interface ClosureModel<S> {

	/**
	 * This method returns the set of points in the model. The returned set can
	 * be modified.
	 * 
	 * @return the set of points in the model.
	 */
	public Set<S> getPoints();
	
	/**
	 * Computes the closure of <code>set</code>.
	 * @param set a set
	 * @return the closure of <code>set</code>
	 */
	public Set<S> closure( Set<S> set );
	
	/**
	 * Computes the external border of <code>set</code>.
	 * 
	 * @param sets a set of points
	 * 
	 * @return the closure of <code>set</code>.
	 */
	public Set<S> getExternalBorder( Set<S> set );
	
	public Set<S> pre( S s );

	public Set<S> post( S s );

	public Set<S> pre( Set<S> set );

	public Set<S> post( Set<S> set );
	
	public Set<S> getSet( String name );

	public Set<S> getInternalBorder( Set<S> set );
	
	public Set<S> getInterior( Set<S> set );
	
	public Set<S> getBorder( Set<S> set );

}
