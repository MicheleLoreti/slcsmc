/**
 * 
 */
package org.cmg.slcsmc.model.graphs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.cmg.slcsmc.model.AbstractClosureModel;

/**
 * @author loreti
 *
 */
public class GraphClosureModel extends AbstractClosureModel<Node>{

	private HashMap<Node,Node> vertexes; 
	private HashMap<String,Set<Node>> labels;
	private LinkedList<Edge> edges;
	private boolean directed; 
	private int edgeCounter;
	
	
	public GraphClosureModel( ) {
		this( true );
	}
	
	public GraphClosureModel(boolean directed ) {
		this.directed = directed;
		this.vertexes = new HashMap<Node,Node>();
		this.labels = new HashMap<String,Set<Node>>();
	}

	public Set<Node> getVertexes() {
		return vertexes.keySet();
	}
	
	public Edge[] getEdgeArrays() {
		return edges.toArray( new Edge[edges.size()] );
	}
	
	private Node addNode( Node n ) {
		Node old = this.vertexes.get(n);
		if (old == null) {
			this.vertexes.put(n,n);
			return n;
		} else {
			return old;
		}
	}
	
	public void addNode( int ... data ) {
		this.addNode( new Node( data ) );
	}

	public boolean addEdge( int[] src , int[] trg ) {
		Node n1 = new Node(src);
		Node n2 = new Node(trg);
		return addEdge( n1 , n2 );
	}
	
	private boolean addEdge( Node src , Node trg ) {
		Node srcNode = vertexes.get(src);
		Node trgNode = vertexes.get(trg); 
		if ((srcNode == null)||(trgNode == null)) {
			throw new IllegalArgumentException();
		}
		if (srcNode.addPost( trgNode )) {
			edgeCounter++;			
			edges.add( new Edge(srcNode,trgNode));
		} else {
			return false;
		}
		trgNode.addPre( srcNode );
		if (!directed) {
			srcNode.addPre( trgNode );
			trgNode.addPost( srcNode );
		}
		return true;
	}

	@Override
	public Set<Node> getPoints() {
		return vertexes.keySet();
	}

	@Override
	public Set<Node> closure(Set<Node> set) {
		HashSet<Node> toReturn = new HashSet<Node>();
		for (Node node : set) {
			toReturn.add(node);
			toReturn.addAll( post(node) );
		}
		return toReturn;
	}

	@Override
	public Set<Node> pre(Node s) {
		return s.getPreset();
	}

	@Override
	public Set<Node> post(Node s) {
		return s.getPoset();
	}

	@Override
	public Set<Node> pre(Set<Node> set) {
		HashSet<Node> toReturn = new HashSet<Node>();
		for (Node n : set) {
			toReturn.addAll( pre(n) );
		}
		return toReturn;
	}

	@Override
	public Set<Node> post(Set<Node> set) {
		HashSet<Node> toReturn = new HashSet<Node>();
		for (Node n : set) {
			toReturn.addAll( post(n) );
		}
		return toReturn;
	}

	
	public void add( String name , Node n ) {
		Node toAdd = vertexes.get(n);
		if (toAdd == null) {
			throw new IllegalArgumentException();
		}
		Set<Node> set = labels.get(name); 
		if (set == null) {
			set = new HashSet<Node>();
			labels.put(name, set);
		}
		set.add( toAdd );
	}
	
	public void add( String name , int[] data ) {
		add( name , new Node( data ) ); 
	}
	
	public void addAll( String name , Iterable<Node> elements ) {
		for (Node n : elements) {
			add( name , n ); 
		}
	}
	
	
	@Override
	public Set<Node> getSet(String name) {
		Set<Node> set = labels.get(name); 
		if (set == null) {
			set = new HashSet<Node>();
		}
		return set;
	}
	
	public void resetCache() {
		this.labels = new HashMap<String,Set<Node>>();
	}

	public int numberOfVertexes() {
		return this.vertexes.size();
	}

	public int numberOfEdges() {
		return edgeCounter;
	}

	public boolean isDirected() {
		return directed;
	}

	public boolean contains(int[] values) {
		return vertexes.containsKey(new Node(values));
	}

	public boolean contains(int[] src , int[] trg ) {
		Node srcNode = vertexes.get(new Node(src));
		return srcNode.getPoset().contains(new Node(trg));
	}

}
