/**
 * 
 */
package org.cmg.slcsmc.model.graphs;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * @author loreti
 *
 */
public class Node {
	
	private int[] data;
	private HashSet<Node> preset;
	private HashSet<Node> poset;
	
	public Node( int ... data ) {
		this.data = data;
		this.preset = new HashSet<Node>();
		this.poset = new HashSet<Node>();
	}
	
	public int getData( int i ) {
		return data[i];
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(data);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Node) {
			return Arrays.equals(data , ((Node) obj).data);
		}
		return false;
	}

	@Override
	public String toString() {
		return Arrays.toString(data);
	}
	
	public Set<Node> getPreset() {
		return preset;
	}

	public Set<Node> getPoset() {
		return poset;
	}

	public boolean addPost(Node trgNode) {
		return poset.add(trgNode);
	}

	public boolean addPre(Node srcNode) {
		return preset.add(srcNode);
	}
	
	public int get( int idx ) {
		return data[idx];
	}
	
}