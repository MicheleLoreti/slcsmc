/**
 * 
 */
package org.cmg.slcsmc.model.graphs;

/**
 * @author loreti
 *
 */
public class Edge {

	private Node src;
	private Node trg;

	public Edge(Node src , Node trg ) {
		this.src = src;
		this.trg = trg;
	}

	/**
	 * @return the src
	 */
	public Node getSrc() {
		return src;
	}

	/**
	 * @return the trg
	 */
	public Node getTrg() {
		return trg;
	}

}
