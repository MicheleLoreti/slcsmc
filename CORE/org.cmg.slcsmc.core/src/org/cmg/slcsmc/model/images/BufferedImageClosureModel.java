/**
 * 
 */
package org.cmg.slcsmc.model.images;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;


/**
 * @author loreti
 *
 */
public class BufferedImageClosureModel extends AbstractImageClosureModel {
	
	protected BufferedImage image;
	
	public BufferedImageClosureModel( BufferedImage image ) {
		this(image,DEFAULT_CLOSURE_MASK);
	}
	
	public  BufferedImageClosureModel( BufferedImage image , int closureMask ) {
		this.image = image;
		initClosureMask(closureMask);
	}
		
	public BufferedImageClosureModel(File file) throws IOException {
		this( ImageIO.read(file) );
	}

	@Override
	public Set<Pixel> getPoints() {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for( int i=0 ; i<getWidth() ; i++ ) {
			for( int j = 0 ; j<getHeight() ; j++ ) {
				toReturn.add( new Pixel( i , j ) );
			}
		}
		return toReturn;
	}

	@Override
	public int getHeight() {
		return image.getHeight();
	}

	@Override
	public int getWidth() {
		return image.getWidth();
	}

	@Override
	public Set<Pixel> closure(Set<Pixel> set) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>( set );
		for (Pixel pixel : set) {
			toReturn.addAll( post( pixel ) );
		}
		return toReturn;
	}

	@Override
	public Set<Pixel> getSet(String name) {
		try {
			Color color = new Color(Integer.parseInt(name));
			HashSet<Pixel> toReturn = new HashSet<Pixel>();
			for( int i=0 ; i<getWidth() ; i++ ) {
				for( int j = 0 ; j<getHeight() ; j++ ) {
					Color pixelColor = new Color( image.getRGB(i, j) );
					int dBlue = Math.abs( color.getBlue()-pixelColor.getBlue() );
					int dRed= Math.abs( color.getRed()-pixelColor.getRed() );
					int dGreen = Math.abs( color.getGreen()-pixelColor.getGreen() );
					if ((dBlue<tollerance)&&(dRed<tollerance)&&(dGreen<tollerance)) {
						toReturn.add( new Pixel( i , j ) );
					}
				}
			}
			return toReturn;
		} catch (NumberFormatException e) {
			return new HashSet<Pixel>();
		}
 	}

	public int getTollerance() {
		return tollerance;
	}

	public void setTollerance(int tollerance) {
		this.tollerance = tollerance;
	}

	public BufferedImage getImage() {
		return image;
	}

	public Dimension getSize() {
		return new Dimension(image.getWidth(),image.getHeight());
	}

	public Dimension getSize(Dimension rv) {
		if (rv == null) {
			return getSize();
		}
		rv.setSize(getSize());
		return rv;
	}
	
	
}
