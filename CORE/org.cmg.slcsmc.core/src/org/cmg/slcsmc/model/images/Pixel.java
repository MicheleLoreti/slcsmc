/**
 * 
 */
package org.cmg.slcsmc.model.images;

/**
 * @author loreti
 *
 */
public class Pixel {
	
	private int x;
	private int y;

	public Pixel(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int hashCode() {
		return (x + y)*(x + y + 1)/2 + y;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pixel) {
			Pixel p = (Pixel) obj;
			return (this.x == p.x)&&(this.y == p.y);
		}
		return false;
	}

	@Override
	public String toString() {
		return "<"+x+","+y+">";
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}


}
