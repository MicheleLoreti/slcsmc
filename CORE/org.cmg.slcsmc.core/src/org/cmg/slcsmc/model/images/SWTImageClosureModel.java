/**
 * 
 */
package org.cmg.slcsmc.model.images;

import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;

/**
 * @author loreti
 *
 */
public class SWTImageClosureModel extends AbstractImageClosureModel {
	
	private ImageData imageData;
	
	private HashMap<String,SWTRGBColorFilter> filters; 
	
	public SWTImageClosureModel( InputStream file ) {
		imageData = new ImageData(file);
		filters = new HashMap<>();
	}
	
	@Override
	public int getHeight() {
		return imageData.height;
	}

	@Override
	public int getWidth() {
		return imageData.width;
	}

	@Override
	public Set<Pixel> getPoints() {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for( int i=0 ; i<imageData.width ; i++ ) {
			for( int j = 0 ; j<imageData.height ; j++ ) {
				toReturn.add( new Pixel( i , j ) );
			}
		}
		return toReturn;
	}

	@Override
	public Set<Pixel> getSet(String name) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		SWTRGBColorFilter filter = filters.get(name);
		if (filter == null) {
			return toReturn;
		}
		for( int i=0 ; i<imageData.width ; i++ ) {
			for( int j = 0 ; j<imageData.height ; j++ ) {
				RGB pixelColor = imageData.palette.getRGB(imageData.getPixel(i, j));
				if (filter.sat(pixelColor)) {
					toReturn.add( new Pixel( i , j ) );
				}
			}
		}
		return toReturn;
 	}
	
	public void setFilter( String name , SWTRGBColorFilter filter ) {
		this.filters.put(name, filter);
	}

	public void setFilter( String name , int r , int g, int b , int delta ) {
		this.filters.put(name, new SWTRGBColorFilter(r, g, b, delta));
	}

	public void setFilter( String name , int x , int y, int delta ) {
		RGB pixelColor = imageData.palette.getRGB(imageData.getPixel(x, y));
		this.filters.put(name, new SWTRGBColorFilter(pixelColor.red, pixelColor.green, pixelColor.blue, delta));
	}

	public ImageData getImageData() {
		return imageData;
	}
	
	
}
