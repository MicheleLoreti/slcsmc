/**
 * 
 */
package org.cmg.slcsmc.model.images;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;

import org.cmg.slcsmc.model.AbstractClosureModel;
import org.cmg.slcsmc.model.ClosureModel;


/**
 * @author loreti
 *
 */
public abstract class AbstractImageClosureModel extends AbstractClosureModel<Pixel>{
	
	/**
	 * 
	 * Added:
	 * setImage(BufferedImage img)
	 */
	protected int tollerance = 50;
	
	private static final int[][] DEFAULT_CLOSURE_MASK_ARRAY = new int[][] {
		new int[] { 0 , -1 } ,
		new int[] { -1 , 0 } ,
		new int[] { 1 , 0 } ,
		new int[] { 0 , 1 }
	};
	
	private static final int[][] EXTENDED_CLOSURE_MASK_ARRAY = new int[][] {
		new int[] { -1 , -1 } ,
		new int[] { -1 , 0 } ,
		new int[] { -1 , 1 } ,
		new int[] { 0 , -1 } ,
		new int[] { 0 , 1 } ,
		new int[] { 1 , -1 } ,
		new int[] { 1 , 0 } ,
		new int[] { 1 , 1 } 
	};
	
	public static final int DEFAULT_CLOSURE_MASK = 0;
	
	public static final int EXTENDED_CLOSURE_MASK = 1;
	
	protected int[][] closureMask ;
	
	protected void initClosureMask(int closureMask) {
		if (closureMask==EXTENDED_CLOSURE_MASK) {
			this.closureMask = EXTENDED_CLOSURE_MASK_ARRAY;
			return ;
		}
		this.closureMask = DEFAULT_CLOSURE_MASK_ARRAY;
		
	}

	@Override
	public Set<Pixel> getPoints() {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for( int i=0 ; i<getWidth() ; i++ ) {
			for( int j = 0 ; j<getHeight() ; j++ ) {
				toReturn.add( new Pixel( i , j ) );
			}
		}
		return toReturn;
	}

	
	public abstract int getHeight();

	public abstract int getWidth();

	@Override
	public Set<Pixel> closure(Set<Pixel> set) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>( set );
		for (Pixel pixel : set) {
			toReturn.addAll( post( pixel ) );
		}
		return toReturn;
	}

	private Pixel shift(Pixel pixel, int[] shift) {
		int x = pixel.getX()+shift[0];
		int y = pixel.getY()+shift[1];
		if ((x<0)||(x>this.getWidth())||(y<0)||(y>this.getHeight())) {
			return null;
		}
		return new Pixel(x,y);
	}

	@Override
	public Set<Pixel> pre(Pixel s) {
		return post( s );
	}

	@Override
	public Set<Pixel> post(Pixel s) {
		HashSet<Pixel> toReturn = new HashSet<Pixel>();
		for (int[] shift : closureMask) {
			Pixel foo = shift( s , shift );
			if (foo !=null ) {
				toReturn.add( foo );
			}
		}
		return toReturn;
	}

	@Override
	public Set<Pixel> pre(Set<Pixel> set) {
		return post( set );
	}

	@Override
	public Set<Pixel> post(Set<Pixel> set) {
		return closure( set );
	}

	@Override
	public abstract Set<Pixel> getSet(String name);

	public int getTollerance() {
		return tollerance;
	}

	public void setTollerance(int tollerance) {
		this.tollerance = tollerance;
	}

}
