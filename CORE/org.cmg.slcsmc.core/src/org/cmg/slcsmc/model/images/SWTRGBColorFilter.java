/**
 * 
 */
package org.cmg.slcsmc.model.images;

import org.eclipse.swt.graphics.RGB;

/**
 * @author loreti
 *
 */
public class SWTRGBColorFilter {
	
	private int red_min;
	
	private int red_max;
	
	private int green_min;
	
	private int green_max;
	
	private int blue_min;
	
	private int blue_max;
	
	public SWTRGBColorFilter( int r , int g , int b ) {
		this( r , g , b , 0 );
	}

	public SWTRGBColorFilter( int r, int g, int b, int delta ) {
		this( Math.max(r-delta,0) , 
				Math.min(r+delta, 255) , 
				Math.max(r-delta,0) , 
				Math.min(r+delta, 255) , 
				Math.max(r-delta,0) , 
				Math.min(r+delta, 255) );
	}

	public SWTRGBColorFilter(int red_min, int red_max, int green_min, int green_max, int blue_min, int blue_max) {
		super();
		this.red_min = red_min;
		this.red_max = red_max;
		this.green_min = green_min;
		this.green_max = green_max;
		this.blue_min = blue_min;
		this.blue_max = blue_max;
	}

	public boolean sat( RGB c ) {
		return (this.red_min<=c.red)&&(c.red<=this.red_max)&&
				(this.green_min<=c.green)&&(c.green<=this.green_max)&&
				(this.blue_min<=c.blue)&&(c.blue<=this.blue_max);
	}
	

}
