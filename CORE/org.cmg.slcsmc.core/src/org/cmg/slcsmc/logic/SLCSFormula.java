/**
 * 
 */
package org.cmg.slcsmc.logic;


/**
 * @author loreti
 *
 */
public interface SLCSFormula {
	
	public <R> R accept( SLCSVisitor<R> visitor );
	
}
