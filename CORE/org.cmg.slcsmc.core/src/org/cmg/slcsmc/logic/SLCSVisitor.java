/**
 * 
 */
package org.cmg.slcsmc.logic;

/**
 * @author loreti
 *
 */
public interface SLCSVisitor<T> {

	
	public T visit( SLCSTrue  formula );

	public T visit( SLCSFalse  formula );

	public T visit( SLCSAtomicProposition formula );
	
	public T visit( SLCSNot formula );

	public T visit( SLCSAnd formula );

	public T visit( SLCSOr formula );

	public T visit( SLCSSurround formula );
	
	public T visit( SLCSDiffusion formula );
	
	public T visit( SLCSIf formula );

	public T visit(SLCSIfAndOnlyIf slcsIfAndOnlyIf);

	public T visit(SLCSBorder slcsBorder);

	public T visit(SLCSExternalBorder slcsExternalBorder);

	public T visit(SLCSInternalBorder slcsInternalBorder);

	public T visit(SLCSGlobally slcsGlobally);

	public T visit(SLCSPossibly slcsPossibly);

	public T visit(SLCSReachability slcsReachability);

	public T visit(SLCSSeparation slcsSeparation);

	public T visit(SLCSClosure slcsClosure);

	public T visit(SLCSInterior slcsInterior);
	
}
