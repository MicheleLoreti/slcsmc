/**
 * 
 */
package org.cmg.slcsmc.logic;


/**
 * @author loreti
 *
 */
public class SLCSInterior implements SLCSFormula {
	
	protected SLCSFormula argument;
	
	

	/**
	 * Creates an SLCSAnd formula.
	 * 
	 * @param left left argument
	 * @param right right argument
	 */
	public SLCSInterior(SLCSFormula argument) {
		super();
		this.argument = argument;
	}



	@Override
	public <R> R accept(SLCSVisitor<R> visitor) {
		return visitor.visit( this );
	}



	public SLCSFormula getArgument() {
		return argument;
	}


}
