/**
 * 
 */
package org.cmg.slcsmc.logic;


/**
 * @author loreti
 *
 */
public class SLCSAtomicProposition implements SLCSFormula {
	
	protected String name;
	

	public SLCSAtomicProposition(String name) {
		super();
		this.name = name;
	}


	@Override
	public <R> R accept(SLCSVisitor<R> visitor) {
		return visitor.visit( this );
	}


	public String getName() {
		return name;
	}

}
