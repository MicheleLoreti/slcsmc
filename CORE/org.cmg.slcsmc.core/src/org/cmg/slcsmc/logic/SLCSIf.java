/**
 * 
 */
package org.cmg.slcsmc.logic;

/**
 * @author loreti
 *
 */
public class SLCSIf implements SLCSFormula {

	private SLCSFormula firstArgument;
	
	public SLCSIf(SLCSFormula firstArgument, SLCSFormula secondArgument) {
		super();
		this.firstArgument = firstArgument;
		this.secondArgument = secondArgument;
	}

	private SLCSFormula secondArgument;

	@Override
	public <R> R accept(SLCSVisitor<R> visitor) {
		return visitor.visit(this);
	}

	public SLCSFormula getFirstArgument() {
		return firstArgument;
	}

	public SLCSFormula getSecondArgument() {
		return secondArgument;
	}

}
