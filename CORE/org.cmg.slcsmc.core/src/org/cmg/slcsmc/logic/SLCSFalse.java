/**
 * 
 */
package org.cmg.slcsmc.logic;


/**
 * @author loreti
 *
 */
public class SLCSFalse implements SLCSFormula {

	@Override
	public <R> R accept(SLCSVisitor<R> visitor) {
		return visitor.visit( this );
	}


}
