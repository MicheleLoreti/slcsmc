/**
 * 
 */
package org.cmg.slcsmc.logic;


/**
 * @author loreti
 *
 */
public class SLCSAnd implements SLCSFormula {
	
	protected SLCSFormula left;
	protected SLCSFormula right;
	
	

	/**
	 * Creates an SLCSAnd formula.
	 * 
	 * @param left left argument
	 * @param right right argument
	 */
	public SLCSAnd(SLCSFormula left, SLCSFormula right) {
		super();
		this.left = left;
		this.right = right;
	}

	@Override
	public <R> R accept(SLCSVisitor<R> visitor) {
		return visitor.visit( this );
	}

	public SLCSFormula getFirstArgument() {
		return left;
	}

	public SLCSFormula getSecondArgument() {
		return right;
	}


}
