/**
 * 
 */
package org.cmg.slcsmc.logic.mc;

import java.util.HashSet;
import java.util.Set;

import org.cmg.slcsmc.logic.SLCSAnd;
import org.cmg.slcsmc.logic.SLCSAtomicProposition;
import org.cmg.slcsmc.logic.SLCSBorder;
import org.cmg.slcsmc.logic.SLCSClosure;
import org.cmg.slcsmc.logic.SLCSDiffusion;
import org.cmg.slcsmc.logic.SLCSExternalBorder;
import org.cmg.slcsmc.logic.SLCSFalse;
import org.cmg.slcsmc.logic.SLCSFormula;
import org.cmg.slcsmc.logic.SLCSGlobally;
import org.cmg.slcsmc.logic.SLCSIf;
import org.cmg.slcsmc.logic.SLCSIfAndOnlyIf;
import org.cmg.slcsmc.logic.SLCSInterior;
import org.cmg.slcsmc.logic.SLCSInternalBorder;
import org.cmg.slcsmc.logic.SLCSNot;
import org.cmg.slcsmc.logic.SLCSOr;
import org.cmg.slcsmc.logic.SLCSPossibly;
import org.cmg.slcsmc.logic.SLCSReachability;
import org.cmg.slcsmc.logic.SLCSSeparation;
import org.cmg.slcsmc.logic.SLCSSurround;
import org.cmg.slcsmc.logic.SLCSTrue;
import org.cmg.slcsmc.logic.SLCSVisitor;
import org.cmg.slcsmc.model.ClosureModel;

/**
 * @author loreti
 *
 */
public class SLCSModelChecker<S> implements SLCSVisitor<Set<S>>{
	
	private ClosureModel<S> model;
	
	public SLCSModelChecker( ClosureModel<S> model ) {
		this.model = model;
	}
	

	public Set<S> sat( SLCSFormula f ) {
		return f.accept( this );
	}
	
	@Override
	public Set<S> visit(SLCSTrue formula) {
		return model.getPoints();
	}

	@Override
	public Set<S> visit(SLCSFalse formula) {
		return new HashSet<S>();
	}

	@Override
	public Set<S> visit(SLCSAtomicProposition formula) {
		return model.getSet( formula.getName() );
	}

	@Override
	public Set<S> visit(SLCSNot formula) {
		Set<S> result = formula.getArgument().accept(this);
		Set<S> toReturn = model.getPoints();
		toReturn.removeAll( result );
		return toReturn;
	}

	@Override
	public Set<S> visit(SLCSAnd formula) {
		Set<S> leftSatSet = formula.getFirstArgument().accept(this);
		leftSatSet.retainAll(formula.getSecondArgument().accept(this));
		return leftSatSet;
	}

	@Override
	public Set<S> visit(SLCSOr formula) {
		Set<S> leftSatSet = formula.getFirstArgument().accept(this);
		leftSatSet.addAll(formula.getSecondArgument().accept(this));
		return leftSatSet;
	}

	@Override
	public Set<S> visit(SLCSSurround formula) {
		Set<S> V = formula.getFirstArgument().accept(this);
		Set<S> Q = formula.getSecondArgument().accept(this);
		return computeSurround( V , Q );
	}

	public Set<S> computeSurround(Set<S> V, Set<S> Q) {
		Set<S> V_Q = new HashSet<S>(  );
		V_Q.addAll( V );
		V_Q.addAll( Q );
		Set<S> T = model.getExternalBorder(V_Q);
		Set<S> A = new HashSet<S>( T );
		A.retainAll(V_Q);
		while (!T.isEmpty()) {
			HashSet<S> nextT = new HashSet<S>();
			for (S x : T) {
				Set<S> N = model.pre( x );
				N.retainAll(V);
				if (!N.isEmpty()) {
					V.removeAll(N);
					N.removeAll(Q);
				}
				nextT.addAll(N);
			}
			T = nextT;
		}	
		return V;
	}
	
	public Set<S> computeReachability( Set<S> V , Set<S> Q ) {
		Set<S> V_Q = new HashSet<S>();
		V_Q.addAll(Q);
		V_Q.retainAll(V);
		Set<S> E = model.pre(V_Q);
		Set<S> R = new HashSet<S>();
		E.addAll(R);
		while (!E.isEmpty()) {
			HashSet<S> newE = new HashSet<S>();
			for (S s : E) {
				R.add(s);
				if (V.contains(s)) {
					V.remove(s);
					newE.add(s);
				}
			}
			E = newE;
		}
		return R;
	}

	public Set<S> computeDiffusion( Set<S> V , Set<S> Q ) {
		Set<S> R = new HashSet<S>();
		R.addAll(V);
		Set<S> E = model.post(V);
		while (!E.isEmpty()) {
			HashSet<S> newE = new HashSet<S>();
			for (S s : E) {
				R.add(s);
				if (Q.contains(s)) {
					V.remove(s);
					newE.add(s);
				}
			}
			E = newE;
		}
		return R;
	}	
	
	@Override
	public Set<S> visit(SLCSDiffusion formula) {
		Set<S> satLeft = formula.getFirstArgument().accept(this);
		Set<S> satRight = formula.getSecondArgument().accept(this);
		return computeDiffusion(satLeft, satRight);
	}


	@Override
	public Set<S> visit(SLCSIf formula) {
		Set<S> leftSet = formula.getFirstArgument().accept(this);
		Set<S> rightSet = formula.getSecondArgument().accept(this);
		Set<S> toReturn = model.getPoints();
		toReturn.removeAll(leftSet);
		toReturn.addAll(rightSet);
		return null;
	}

	@Override
	public Set<S> visit(SLCSIfAndOnlyIf formula) {
		Set<S> leftSet = formula.getFirstArgument().accept(this);
		Set<S> rightSet = formula.getSecondArgument().accept(this);
		Set<S> ifSet = model.getPoints();
		ifSet.removeAll(leftSet);
		ifSet.addAll(rightSet);
		Set<S> onlyIfSet = model.getPoints();
		onlyIfSet.removeAll(rightSet);
		onlyIfSet.addAll(leftSet);
		ifSet.retainAll(onlyIfSet);
		return ifSet;
	}


	@Override
	public Set<S> visit(SLCSBorder slcsBorder) {
		return model.getBorder(slcsBorder.getArgument().accept(this));
	}


	@Override
	public Set<S> visit(SLCSExternalBorder slcsExternalBorder) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Set<S> visit(SLCSInternalBorder slcsInternalBorder) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Set<S> visit(SLCSGlobally slcsGlobally) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Set<S> visit(SLCSPossibly slcsPossibly) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Set<S> visit(SLCSReachability slcsReachability) {
		Set<S> left = model.getPoints();
		left.removeAll(slcsReachability.getFirstArgument().accept(this));
		Set<S> right = model.getPoints();
		right.removeAll(slcsReachability.getSecondArgument().accept(this));
		Set<S> toReturn = model.getPoints();
		toReturn.removeAll(computeSurround(right, left));
		return toReturn;
	}


	@Override
	public Set<S> visit(SLCSSeparation slcsSeparation) {
		Set<S> left = model.getPoints();
		left.removeAll(slcsSeparation.getFirstArgument().accept(this));
		Set<S> right = model.getPoints();
		right.removeAll(slcsSeparation.getSecondArgument().accept(this));
		Set<S> toReturn = model.getPoints();
		toReturn.removeAll(computeDiffusion(left, right));
		return toReturn;
	}


	@Override
	public Set<S> visit(SLCSClosure slcsClosure) {
		return computeDiffusion(slcsClosure.getArgument().accept(this), new HashSet<S>());
	}


	@Override
	public Set<S> visit(SLCSInterior slcsInterior) {
		return null;
	}

}
